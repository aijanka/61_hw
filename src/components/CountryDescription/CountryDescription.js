import React from 'react';
import './CountryDescription.css';

const CountryDescription = props => {
    let borders = '';
    if(props.border !== 'no borders') {
        borders = props.border.map((country, index) => <li key={index}>{country}</li>)
    } else borders = props.border;

    return (
        <div className="DescriptionContainer">
            <h1 className="CountryName">{props.name}</h1>
            <p><b>Capital:</b> {props.capital} </p>
            <p><b>Population:</b> {props.population} </p>
            <p>Borders</p>
            <ul>
                {borders}
            </ul>

        </div>
    )
};

export default CountryDescription;